FROM python:3.6-alpine3.8

RUN apk add --no-cache \
        bash \
        build-base \
	ca-certificates \
	cyrus-sasl-dev \
	graphviz \
	jpeg-dev \
	libffi-dev \
	libxml2-dev \
	libxslt-dev \
	openldap-dev \
	postgresql-dev \
        curl && \
        update-ca-certificates
	
WORKDIR /opt

ARG BRANCH=master
ARG URL=https://gitlab.com/gole-group/core/inventory-manager/-/archive/$BRANCH/inventory-manager-$BRANCH.tar.gz
RUN curl -o - "${URL}" | \
    tar xz ; mv inventory-manager-* hi-manager

WORKDIR /opt/hi-manager
RUN pip3 install -r requirements.txt

COPY docker/nginx.conf /etc/hi-manager-nginx/nginx.conf
COPY docker/docker-entrypoint.sh docker-entrypoint.sh
COPY docker/settings_docker.py goleHI/goleHI/settings.py
COPY configuration/gunicorn_config.py gunicorn_config.py

WORKDIR /opt/hi-manager/goleHI/

ENTRYPOINT [ "/opt/hi-manager/docker-entrypoint.sh" ]

CMD [ "gunicorn", "-c", "/opt/hi-manager/gunicorn_config.py", "goleHI.wsgi" ]
