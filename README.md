 gole-hi-manager-docker
========================

Components needed to build *gole-hi-manager* as a Docker container.

* [Source code](https://gitlab.com/gole-group/core/inventory-manager)

* [Official Documentation](https://inventory-manager.readthedocs.io/en/stable/)

* [Official Docker Images](https://hub.docker.com/r/gole/hi-manager)

## Starting docker gole-hi-manager

This image have a postgres dependencies. You need start with `docker-compose`.

```
$ git clone -b master https://gitlab.com/gole-group/gole-community/docker/gole-hi-manager-docker.git
$ cd gole-hi-manager-docker

 - # make your changes if you want

$ docker-compose up -d
$ curl http://localhost:8080
```

### Default Credentials

* user: admin
* pass: golehimanager
