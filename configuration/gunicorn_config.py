command = '/usr/bin/gunicorn'
pythonpath = '/opt/hi-manager/goleHI/'
bind = '0.0.0.0:8001'
workers = 3
user = 'root'
errorlog = '-'
accesslog = '-'
capture_output = False
